import java.util.*;
//https://bitbucket.org/ihinno/homework5/src/5a038c97b26e16ec56e279381f8ae5a6343ab055/src/TreeNode.java?at=master&fileviewer=file-view-default
//http://enos.itcollege.ee/~jpoial/algoritmid/puud.html 

public class TreeNode {

	// tipp
	private String name;

	// tipu esmiene alluv
	private TreeNode firstChild;

	// tipu parempoolne naaber
	private TreeNode nextSibling;

	// konstruktor
	TreeNode() {

	}

	// tipu konstruktor
	public TreeNode(String n, TreeNode d, TreeNode r) {
		this.name = n;
		this.firstChild = d;
		this.nextSibling = r;

	}

	public TreeNode(String n) {
		this.name = n;
		this.firstChild = null;
		this.nextSibling = null;
	}

	public static TreeNode parsePrefix(String s) {
		
	
		s = s.trim();
		// t�hja ja null v�listamine
		if (s == null || s.isEmpty()) {
			throw new RuntimeException("Ei saa v�ljastada t�hja puud");
		}
		StringBuffer b = new StringBuffer();
		TreeNode root = new TreeNode();
		TreeNode currentNode = root;
		List<TreeNode> currentNodeParents = new ArrayList<TreeNode>();
		for (int i = 0; i < s.length(); i++) {
			char currentChar = s.charAt(i);
			if (currentChar == '(') {
				if (currentNode.name == null) {
					currentNode.name = b.toString();
					if (currentNode.name.isEmpty() || currentNode.name.contains(" ") || currentNode.equals("(") || currentNode.equals(",") || currentNode.equals(")")) {
						throw new RuntimeException("T�hi nimi v�i t�hikud nimes");
					}
					b.setLength(0);
				}
				if (currentNode.firstChild == null) {
        		    // Kui tipul pole alamaid, siis lisame uue tipu esimeseks alamaks
				    currentNode.setFirstChild(new TreeNode());
                	currentNodeParents.add(currentNode);
					currentNode = currentNode.getFirstChild();
				} else {
				    // Kui tipule on juba alam lisatud, siis peab olema s�ntaksi viga kujul A(B)(C)
					throw new RuntimeException("Vale struktuuriga puu");
				}
			} else if (currentChar == ')') {
				if (currentNode.name == null) {
					currentNode.name = b.toString();
					if (currentNode.name.isEmpty() || currentNode.name.contains(" ")) {
						throw new RuntimeException("T�hi nimi v�i t�hikud nimes");
					}
					b.setLength(0);
				}
				currentNode = currentNodeParents.remove(currentNodeParents.size() - 1);
			} else if (currentChar == ',') {
				if (currentNode.name == null) {
					currentNode.name = b.toString();
					if (currentNode.name.isEmpty() || currentNode.name.contains(" ")) {
						throw new RuntimeException("T�hi nimi v�i t�hikud nimes");
					}
					b.setLength(0);
				}
				currentNode.setNextSibling(new TreeNode());
				currentNode = currentNode.getNextSibling();
			} else {
				b.append(currentChar);
			}
		}
		if (currentNode.name == null) {
			currentNode.name = b.toString();
			if (currentNode.name == "" || currentNode.name.contains(" ")) {
				throw new RuntimeException("T�hi nimi v�i t�hikud nimes");
			}
			b.setLength(0);
		}
		if (!currentNode.name.equals(root.name)) {
			throw new RuntimeException("Vale struktuuriga puu");
		}

		return root;
	}



	public String rightParentheticRepresentation() {

		StringBuilder b = new StringBuilder();
		TreeNode current = this.firstChild;
		if (current != null) {
			b.append("(");
			while (current != null) {
				b.append(current.rightParentheticRepresentation());
				current = current.nextSibling;
				if (current != null)
					b.append(",");
			}
			b.append(")");
		}
		b.append(name);
		return b.toString();

	}

	public static void main(String[] param) {
		String s = "A(B1,C,D)";
		TreeNode t = TreeNode.parsePrefix(s);
		String v = t.rightParentheticRepresentation();
		System.out.println("Tree: " + s + " ==> " + v); // A(B1,C,D) ==> (B1,C,D)A
		
		
				
		String s3 = "+(*(-(512,1),4),/(-6,3))";
		TreeNode t4 = TreeNode.parsePrefix(s3);
		String v4 = t4.rightParentheticRepresentation();
		System.out.println("Tree: " + s3 + " ==> " + v4);
		
				
		String s2 = "6(5(1,3(2),4))";
		TreeNode t2 = TreeNode.parsePrefix(s2);
		String v2 = t2.rightParentheticRepresentation();
		System.out.println("Tree: " + s2 + " ==> " + v2);
		
		
	//vead
		
		//String s5 = "abc(def(ghi)(jkl))";
		String s5 = "abc(def(ghi, jkl))";
//
		TreeNode t3 = TreeNode.parsePrefix(s5);
		String v3 = t3.rightParentheticRepresentation();
		System.out.println("Tree: " + s5 + " ==> " + v3); 
		
		
	}

	// tagastab alluva
	public TreeNode getFirstChild() {
		return firstChild;
	}

	public void setFirstChild(TreeNode firstChild) {
		this.firstChild = firstChild;
	}

	// tagastab naabri
	public TreeNode getNextSibling() {
		return nextSibling;
	}

	public void setNextSibling(TreeNode nextSibling) {
		this.nextSibling = nextSibling;
	}

	// tagstab nime
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}